Sample application with Spring Boot 2 and Waffle for MS domain authentication.

Configured for Spring Form authentication and Waffle Negotiate in only one URL. Frontend should use Negotiate on such URL and fallback to form if unable.

Boot and browse http://localhost:8080/

By default, it allows ROLE_USER users (which is added by default by Waffle).
