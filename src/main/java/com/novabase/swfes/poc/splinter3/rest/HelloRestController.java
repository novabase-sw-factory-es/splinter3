package com.novabase.swfes.poc.splinter3.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Slf4j
@RestController
public class HelloRestController {

  @GetMapping("/me")
  @PreAuthorize("hasRole('ROLE_USER')")
  public String hello(Authentication auth) {
    log.debug("Authenticated as {} with roles {}", auth.getPrincipal(), auth.getAuthorities());
    return "Hello " + auth.getPrincipal() + " with roles " + auth.getAuthorities();
  }

  @GetMapping("/time")
  @PreAuthorize("hasRole('ROLE_USER')")
  public String time(Authentication auth) {
    log.debug("Authenticated as {}", auth.getPrincipal());
    return "It is " + new Date() + " and you are " + auth.getPrincipal();
  }

}

