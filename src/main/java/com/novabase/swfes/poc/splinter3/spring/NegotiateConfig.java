package com.novabase.swfes.poc.splinter3.spring;

import com.novabase.swfes.poc.splinter3.security.LoggedUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import waffle.spring.NegotiateSecurityFilter;
import waffle.spring.NegotiateSecurityFilterEntryPoint;

@Configuration
@Order(1)
@Slf4j
@RestController
public class NegotiateConfig extends WebSecurityConfigurerAdapter {

  private static final String NEGOTIATE_LOGIN_URL = "/negotiateLogin";

  @Autowired
  private NegotiateSecurityFilter negotiateSecurityFilter;

  @Autowired
  private NegotiateSecurityFilterEntryPoint negotiateSecurityFilterEntryPoint;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off
    http
      .requestMatcher(new AntPathRequestMatcher(NEGOTIATE_LOGIN_URL))
      .authorizeRequests()
        .anyRequest()
        .authenticated()
        .and()
      .httpBasic()
        .authenticationEntryPoint(negotiateSecurityFilterEntryPoint)
        .and()
      .addFilterBefore(negotiateSecurityFilter, BasicAuthenticationFilter.class)
    ;
    // @formatter:on
  }

  @GetMapping(NEGOTIATE_LOGIN_URL)
  public LoggedUser negotiateLogin(Authentication authentication) {
    LoggedUser loggedUser = LoggedUser.fromAuthentication(authentication);
    log.debug("LoggedUser: {}", loggedUser);
    return loggedUser;
  }

}
