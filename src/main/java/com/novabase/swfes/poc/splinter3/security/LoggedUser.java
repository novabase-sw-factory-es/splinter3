package com.novabase.swfes.poc.splinter3.security;

import java.util.stream.Collectors;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import waffle.servlet.WindowsPrincipal;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class LoggedUser {
  @NonNull
  String username;
  @NonNull
  List<String> roles;

  public static LoggedUser fromAuthentication(Authentication authentication) {
    Object principal = authentication.getPrincipal();
    WindowsPrincipal user = (WindowsPrincipal) principal;
    List<String> roles = authentication.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());
    LoggedUser loggedUser = new LoggedUser(user.getName(), roles);
    return loggedUser;
  }

}
