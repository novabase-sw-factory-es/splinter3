package com.novabase.swfes.poc.splinter3.spring;

import com.novabase.swfes.poc.splinter3.security.LoggedUserProviderAuthenticationSuccessHandler;
import com.novabase.swfes.poc.splinter3.security.Status200LogoutSuccessHandler;
import com.novabase.swfes.poc.splinter3.security.Status401AuthenticationFailureHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@Order(2)
public class FormSecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String FAVICON_URL = "/favicon.ico";
  private static final String LOGIN_URL = "/login";
  private static final String LOGOUT_URL = "/logout";
  private static final String SESSION_COOKIE = "JSESSIONID";

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off
    http
      .exceptionHandling()
        .authenticationEntryPoint(restAuthenticationEntryPoint())
        .and()
      .authorizeRequests()
        .antMatchers(FAVICON_URL).permitAll()
        .anyRequest().authenticated()
        .and()
      .formLogin()
        .loginProcessingUrl(LOGIN_URL)
        .permitAll()
        .successHandler(restAuthenticationSuccessHandler())
        .failureHandler(restAuthenticationFailureHandler())
        .and()
      .logout()
        .logoutUrl(LOGOUT_URL)
        .deleteCookies(SESSION_COOKIE)
        .permitAll()
        .logoutSuccessHandler(logoutSuccessHandler())
        .and()
      .csrf()
        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
        .disable()
      ;
    // @formatter:on
  }

  @Bean
  public AuthenticationEntryPoint restAuthenticationEntryPoint() {
    return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
  }

  @Bean
  public AuthenticationSuccessHandler restAuthenticationSuccessHandler() {
    return new LoggedUserProviderAuthenticationSuccessHandler();
  }

  @Bean
  public AuthenticationFailureHandler restAuthenticationFailureHandler() {
    return new Status401AuthenticationFailureHandler();
  }

  @Bean
  public LogoutSuccessHandler logoutSuccessHandler() {
    return new Status200LogoutSuccessHandler();
  }

}
