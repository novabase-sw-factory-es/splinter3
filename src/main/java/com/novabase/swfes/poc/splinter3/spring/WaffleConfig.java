package com.novabase.swfes.poc.splinter3.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import waffle.servlet.spi.NegotiateSecurityFilterProvider;
import waffle.servlet.spi.SecurityFilterProvider;
import waffle.servlet.spi.SecurityFilterProviderCollection;
import waffle.spring.NegotiateSecurityFilter;
import waffle.spring.NegotiateSecurityFilterEntryPoint;
import waffle.spring.WindowsAuthenticationProvider;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;

import java.util.List;

@Configuration
public class WaffleConfig {

  @Bean
  public WindowsAuthProviderImpl windowsAuthProviderImpl() {
    return new WindowsAuthProviderImpl();
  }

  @Bean
  public WindowsAuthenticationProvider windowsAuthenticationProvider(WindowsAuthProviderImpl windowsAuthProvider) {
    WindowsAuthenticationProvider windowsAuthenticationProvider = new WindowsAuthenticationProvider();
    windowsAuthenticationProvider.setAuthProvider(windowsAuthProvider);
    return windowsAuthenticationProvider;
  }

  @Bean
  public NegotiateSecurityFilterProvider negotiateSecurityFilterProvider(WindowsAuthProviderImpl windowsAuthProvider) {
    NegotiateSecurityFilterProvider negotiateSecurityFilterProvider = new NegotiateSecurityFilterProvider(windowsAuthProvider);
    negotiateSecurityFilterProvider.setProtocols(List.of("Negotiate"));
    return negotiateSecurityFilterProvider;
  }

  @Bean
  public SecurityFilterProviderCollection securityFilterProviderCollection(NegotiateSecurityFilterProvider negotiateSecurityFilterProvider) {
    SecurityFilterProvider[] securityFilterProviders = {negotiateSecurityFilterProvider};
    return new SecurityFilterProviderCollection(securityFilterProviders);
  }

  @Bean
  public NegotiateSecurityFilterEntryPoint negotiateSecurityFilterEntryPoint(SecurityFilterProviderCollection securityFilterProviderCollection) {
    NegotiateSecurityFilterEntryPoint negotiateSecurityFilterEntryPoint = new NegotiateSecurityFilterEntryPoint();
    negotiateSecurityFilterEntryPoint.setProvider(securityFilterProviderCollection);
    return negotiateSecurityFilterEntryPoint;
  }

  @Bean
  public NegotiateSecurityFilter negotiateSecurityFilter(SecurityFilterProviderCollection securityFilterProviderCollection) {
    NegotiateSecurityFilter negotiateSecurityFilter = new NegotiateSecurityFilter();
    negotiateSecurityFilter.setProvider(securityFilterProviderCollection);
    return negotiateSecurityFilter;
  }

}
